#!/usr/bin/python
#
# Copyright (c) 2016 Contributors as noted in the AUTHORS file
#
# This file is part of KTF.

#  KTF is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Lesser General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.

#  KTF is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Lesser General Public License for more details.

#  You should have received a copy of the GNU Lesser General Public License
#  along with KTF.  If not, see <http://www.gnu.org/licenses/>.


from BaseHTTPServer import BaseHTTPRequestHandler,HTTPServer
import glob
import os

PORT_NUMBER = 5555

#This class will handles any incoming request from
#the browser

#This class will handles any incoming request from
#the browser 
class myHandler(BaseHTTPRequestHandler):
	
	#Handler for the GET requests
	def do_GET(self):
		self.send_response(200)
		self.send_header('Content-type','text/html')
		self.end_headers()
		# Send the html message
		self.wfile.write("Hello World ! <BR>")
                files = filter(os.path.isdir, os.listdir(os.getcwd()))
                for f in files:
                   self.wfile.write("%s<br>"  % f)
		return

class server():
	
    def __init__(self):
	#Create a web server and define the handler to manage the
	#incoming request
	self.server = HTTPServer(('', PORT_NUMBER), myHandler)
	print 'Started httpserver on port ' , PORT_NUMBER
	
	#Wait forever for incoming htto requests
	self.server.serve_forever()

    def close(self):
	print '^C received, shutting down the web server'
	self.server.socket.close()
	


if __name__ == "__main__":
    try:
        K = server()
    except KeyboardInterrupt:
        K.close()
